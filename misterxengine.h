#ifndef MISTERXENGINE_H
#define MISTERXENGINE_H

#include "MisterxUser.h"
#include "qxmpp/source/QXmppInvokable.h"
#include <QTimer>
#include <QPointF>


class MisterxEngine : public QXmppInvokable
{

        Q_OBJECT

public:
        MisterxEngine(QObject *parent=0);
        ~MisterxEngine();
        MisterxUser *localUser;
        bool isAuthorized( const QString &jid ) const;
        QTimer* updateTimer;
        QList<QString> playerList;
        QHash<QString,MisterxUser*> playerhash;
public slots:
        void rupdateSettings(QString  playerList, QString area, QString time);
        void rupdateGameDat(QString jid,QString coordinates);
        void rstartGame();
        void rstopGame(QString win);

signals:
        void gameStarted();
        void gameStoped();
        void gameUpdated(MisterxUser*);
        void timerEnded(QString coordinates);

private slots:
        void timerEnd(void);

};

#endif // MISTERXENGINE_H
