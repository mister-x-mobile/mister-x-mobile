#ifndef MISTERXSETTINGSFORM_H
#define MISTERXSETTINGSFORM_H

#include <QWidget>

namespace Ui {
    class MisterXSettingsForm;
}

class MisterXSettingsForm : public QWidget {
    Q_OBJECT
public:
    MisterXSettingsForm(QWidget *parent = 0);
    ~MisterXSettingsForm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MisterXSettingsForm *ui;

private slots:
    void on_cancelButton_clicked();
    void on_saveButton_clicked();
};

#endif // MISTERXSETTINGSFORM_H
