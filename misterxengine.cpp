#include "misterxengine.h"
#include <QTimer>
#include <QPointF>
#include <iostream>
#include <QDebug>
#include <QHash>

MisterxEngine::MisterxEngine(QObject *parent)
:QXmppInvokable(parent)
{
    localUser= new MisterxUser(this);
    updateTimer = new QTimer();
    updateTimer->setInterval(5000);
    //updateTimer->stop();
    connect(updateTimer,SIGNAL(timeout()), this, SLOT(timerEnd()));
}

MisterxEngine::~MisterxEngine()
{


}

bool MisterxEngine::isAuthorized( const QString &jid ) const
{
    return true;
}

void MisterxEngine::rupdateSettings(QString playerString, QString area, QString time)
{
    this->playerList=playerString.split(" ",QString::SkipEmptyParts);
    int i=1;
    foreach(QString player,playerList){
        if(player==localUser->jid){
            playerhash.insert(player,localUser);
        }
        else{
            MisterxUser * user=new MisterxUser;
            playerhash.insert(player,user);
        }
        playerhash[player]->jid=player;
        playerhash[player]->coordinates.clear();
        playerhash[player]->coordinates.append(7);
        playerhash[player]->coordinates.append(20);
        if(i) {
            playerhash[player]->role=MisterxUser::Misterx;
            i=0;
        }
        else  playerhash[player]->role=MisterxUser::Player;
    }
    qDebug() << "Settings erhalten" ;
}

void MisterxEngine::rupdateGameDat(QString jid,QString coordinates)
{
    qDebug() << "Daten erhalten";
    if(!coordinates.isEmpty()){
        QStringList coordtable=coordinates.split(" ",QString::SkipEmptyParts);
        playerhash[jid]->coordinates.clear();
        foreach(QString coord,coordtable){
            playerhash[jid]->coordinates.append(coord.toDouble());

        }

    }
      emit gameUpdated(playerhash[jid]);
}

void MisterxEngine::rstartGame()
{
    updateTimer->start();
    emit gameStarted();
}
void MisterxEngine::rstopGame(QString win)
{
    updateTimer->stop();
    emit gameStoped();
}

void MisterxEngine::timerEnd(void)
{
    //GPS daten auslesen und in local user speichern.
    QString coordinatesString;
    foreach(double coord,localUser->coordinates){
    coordinatesString.append(QString::number(coord));
    coordinatesString.append(" ");
    }
    emit timerEnded(coordinatesString);
}

