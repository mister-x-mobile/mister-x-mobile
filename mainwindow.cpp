#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "misterxclient.h"
#include <QtGui>
 #include <QStringListModel>
#include <QStringList>
#include <QTimer>


MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    engine = new MisterxEngine(this);
    client = new MisterXClient(engine,this);
    client->addInvokableInterface(engine);
    connect(client,SIGNAL(presenceReceived(QXmppPresence)),this,SLOT(refresh()));
    connect(client,SIGNAL(presenceReceived(QXmppPresence)),client,SLOT(updatePlayerList()));
    connect(engine,SIGNAL(timerEnded(QString)),client,SLOT(updateGameDat(QString)));
    connect(engine,SIGNAL(gameStarted()),this,SLOT(stoprefresh()));
    connect(engine,SIGNAL(gameUpdated(MisterxUser*)),this,SLOT(showUserdata(MisterxUser*)));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::connectClient()
{
    client->makeConnection();
}

void MainWindow::on_backButton_clicked()
{

    client->disconnect();
    engine->playerList.clear();
    client->onlineList.clear();

    QStringListModel * listmodel = new QStringListModel();
    listmodel->setStringList(client->onlineList);
    ui->playerList->setModel(listmodel);
    ui->playerList->setViewMode(QListView::ListMode);

    QStringListModel * listmodel2 = new QStringListModel();
    listmodel2->setStringList(engine->playerList);
    ui->addedView->setModel(listmodel2);
    ui->addedView->setViewMode(QListView::ListMode);


    this->hide();
}


void MainWindow::refresh()
{
    client->createOnlineList();
    QStringListModel * listmodel = new QStringListModel();
    listmodel->setStringList(client->onlineList);
    ui->playerList->setModel(listmodel);
    ui->playerList->setViewMode(QListView::ListMode);

    client->updatePlayerList();
    QStringListModel * listmodel2 = new QStringListModel();
    listmodel2->setStringList(engine->playerList);
    ui->addedView->setModel(listmodel2);
    ui->addedView->setViewMode(QListView::ListMode);
}




void MainWindow::on_playerList_doubleClicked(QModelIndex index)
{
    client->addPlayer(index);
    QStringListModel * listmodel = new QStringListModel();
    listmodel->setStringList(engine->playerList);
    ui->addedView->setModel(listmodel);
    ui->addedView->setViewMode(QListView::ListMode);
}

void MainWindow::on_addedView_doubleClicked(QModelIndex index)
{
    client->removePlayer(index);
    QStringListModel * listmodel = new QStringListModel();
    listmodel->setStringList(engine->playerList);
    ui->addedView->setModel(listmodel);
    ui->addedView->setViewMode(QListView::ListMode);
}

void MainWindow::on_startButton_clicked()
{
    QList<QString> area;
    area.append("1");
    area.append("2");
    area.append("3");
    client->updateSettings(engine->playerList,area,"10");
    client->startGame();
    //this->hide();
}
void MainWindow::stoprefresh()
{
    disconnect(client,SIGNAL(presenceReceived(QXmppPresence)),this,SLOT(refresh()));
    disconnect(client,SIGNAL(presenceReceived(QXmppPresence)),client,SLOT(updatePlayerList()));
}

void MainWindow::showUserdata(MisterxUser * data)
{
    qDebug() << "showUserdata";
    QStringListModel * listmodel = new QStringListModel();
    coord.append(data->jid);
    foreach(double num,data->coordinates){
        coord.append(QString::number(num));
            }
    listmodel->setStringList(coord);
    ui->controlView->setModel(listmodel);
    ui->controlView->setViewMode(QListView::ListMode);
}
