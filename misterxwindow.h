#ifndef MISTERXWINDOW_H
#define MISTERXWINDOW_H

#include <QMainWindow>
#include "mainwindow.h"
#include "misterxsettingsform.h"

namespace Ui {
    class MisterXWindow;
}

class MisterXWindow : public QWidget {
    Q_OBJECT
public:
    MisterXWindow(QWidget *parent = 0);
    ~MisterXWindow();

public slots:
    void on_hostButton_clicked();
    void on_joinButton_clicked();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MisterXWindow *ui;
    MainWindow *settings;
    MisterXSettingsForm *config;

private slots:
    void on_settingsButton_clicked();
};

#endif // MISTERXWINDOW_H
