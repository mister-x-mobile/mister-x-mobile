#include "misterxclient.h"

#include <iostream>
#include <QSettings>

#include "qxmpp/source/QXmppLogger.h"
#include "qxmpp/source/QXmppConfiguration.h"
#include "qxmpp/source/QXmppRoster.h"
#include "qxmpp/source/QXmppInvokable.h"

#include "misterxengine.h"

#include "qxmpp/source/QXmppRemoteMethod.h"




MisterXClient::MisterXClient(MisterxEngine *xengine,QObject *parent)
{
    //initialize
    numberOfPlayers=0;
    engine=xengine;


}

MisterXClient::~MisterXClient()
{

}

void MisterXClient::makeConnection() {
    QXmppLogger::getLogger()->setLoggingType(QXmppLogger::STDOUT);
    QSettings settings;
    QXmppConfiguration config;
    config.loadFrom(&settings);
    config.setUseSASLAuthentication( false );
    config.setAutoReconnectionEnabled(TRUE);
    config.setResource("mister-x");
    this->connectToServer(config,QXmppPresence::Available); //password required
    engine->localUser->jid=config.getJidBare();
}

/*Saves avaiable players in onlineList*/

void MisterXClient::createOnlineList(void){
    QXmppRoster& roster=this->getRoster();
    QString qnames;         //saves Jids
    entries=roster.getRosterEntries();
    onlineList.clear();

    QString ownbarejid=this->getConfiguration().getJidBare();
    this->onlineList.append(ownbarejid);
    if(!engine->playerList.contains(ownbarejid)){
        engine->playerList.append(ownbarejid);
    }

    //namelist saves all Jids with presence not unavailable and ongame as statustext
    foreach (QXmppRoster::QXmppRosterEntry entry, entries){
        qnames = entry.getBareJid();
        QStringList resource = roster.getResources(qnames);
        if(resource.contains("mister-x")){
                QXmppPresence presence=roster.getPresence(qnames,"mister-x");
                if((!(presence.getType()==QXmppPresence::Unavailable)) ){
                        std::string names;
                        names = qnames.toStdString();
                        std::cout << "You can play with " << names << std::endl;
                        onlineList.append(qnames);
                    }
       }
    }
}

void MisterXClient::addPlayer(QModelIndex player){
    numberOfPlayers++;
    int number= player.row();
    QString name=this->onlineList[number];
    if(!engine->playerList.contains(name)){
        engine->playerList.append(name);
        std::cout << "Number " << number << " added to Playerlist" << std::endl;
    }
}

void MisterXClient::removePlayer(QModelIndex player){
    if(player.row()>0){
        numberOfPlayers--;
        int number= player.row();
        engine->playerList.removeAt(number);
        std::cout << "Number " << number << " deleted from Playerlist" << std::endl;
    }
}

void MisterXClient::updatePlayerList(){

    for(int i=0;i<engine->playerList.length();i++){
        QString barejid=engine->playerList.at(i);
        if(!onlineList.contains(barejid)){
                engine->playerList.removeAt(i);
            }
    }
}




/////////////////////////////////////////////////remote////////////////////////////////////////////////////////////

void MisterXClient::updateSettings(QList<QString> playerList, QList<QString> areaList, QString timeString)
{
    QString playerString;
    foreach(QString player,playerList){
        playerString.append(player);
        playerString.append(" ");
    }
     QString areaString;
    foreach(QString area,areaList){
        areaString.append(area);
        areaString.append(" ");
    }
    foreach(QString player,engine->playerList){
        QString barejid=player;
        barejid.append("/mister-x");
        QXmppRemoteMethodResult methodResult = callRemoteMethod(barejid, "MisterxEngine.rupdateSettings", playerString, areaString,timeString);
    if( methodResult.hasError )
        error( methodResult.code, methodResult.errorMessage );
    else
        result( methodResult.result );

    }
}


void MisterXClient::updateGameDat(QString coordinates)
{

    foreach(QString player,engine->playerList){
        QString barejid=player;
        std::cout << "player" << barejid.toStdString() <<  std::endl;
        barejid.append("/mister-x");
        QString ownjid=this->getConfiguration().getJidBare();
        QXmppRemoteMethodResult methodResult = callRemoteMethod(barejid, "MisterxEngine.rupdateGameDat", ownjid,coordinates);
    if( methodResult.hasError )
        error( methodResult.code, methodResult.errorMessage );
    else
        result( methodResult.result );
    }
}

void MisterXClient::startGame()
{
    foreach(QString player,engine->playerList){
        QString barejid=player;
        barejid.append("/mister-x");
        QXmppRemoteMethodResult methodResult = callRemoteMethod(barejid, "MisterxEngine.rstartGame" );
    if( methodResult.hasError )
        error( methodResult.code, methodResult.errorMessage );
    else
        result( methodResult.result );
    }
}

void MisterXClient::stopGame(QString win)
{
    foreach(QString player,engine->playerList){
        QString barejid=player;
        barejid.append("/mister-x");
        QXmppRemoteMethodResult methodResult = callRemoteMethod(barejid, "MisterxEngine.rstopGame", win );
    if( methodResult.hasError )
        error( methodResult.code, methodResult.errorMessage );
    else
        result( methodResult.result );
    }

}

void MisterXClient::result(const QVariant &value )
{
    qDebug() << "Result:" << value;
}

void MisterXClient::error( int code, const QString &message )
{
    qDebug() << "Error:" << code << message;
}
