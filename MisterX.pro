# -------------------------------------------------
# Project created by QtCreator 2009-11-11T13:13:13
# -------------------------------------------------
QT += network \
    webkit \
    xml
TARGET = MisterX
TEMPLATE = app
SOURCES += main.cpp \
    misterxwindow.cpp \
    location.cpp \
    misterxclient.cpp \
    mainwindow.cpp \
    misterxengine.cpp \
    MisterxUser.cpp \
    misterxsettingsform.cpp
HEADERS += misterxwindow.h \
    location.h \
    misterxclient.h \
    mainwindow.h \
    misterxengine.h \
    MisterxUser.h \
    misterxsettingsform.h
FORMS += misterxwindow.ui \
    mainwindow.ui \
    misterxsettingsform.ui
OTHER_FILES += 
LIBS += qxmpp/source/debug/libQXmppClient_d.a
