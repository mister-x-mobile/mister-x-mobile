#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include "ui_mainwindow.h"
#include "misterxclient.h"
#include <QStringListModel>
#include "misterxengine.h"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

private:
    //QStringListModel *listmodel;

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    MisterXClient *client;
    MisterxEngine *engine;
public slots:
    void connectClient();


private:
    Ui::MainWindow *ui;
    QStringList coord;


private slots:
    void on_startButton_clicked();
    void on_addedView_doubleClicked(QModelIndex index);
    void on_playerList_doubleClicked(QModelIndex index);
    void on_backButton_clicked();
    void refresh();
    void stoprefresh();
    void showUserdata(MisterxUser*);

};

#endif // MAINWINDOW_H
