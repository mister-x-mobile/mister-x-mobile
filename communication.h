#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include <QObject>
#include "qxmpp/source/QXmppMessage.h"
#include "qxmpp/source/QXmppLogger.h"
#include "qxmpp/source/QXmppClient.h"
#include "qxmpp/source/QXmppRoster.h"

class Communication : public QObject
{
    Q_OBJECT
public:
    Communication();
};

#endif // COMMUNICATION_H
