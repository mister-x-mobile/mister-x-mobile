#ifndef QXMPPMISTERXIQ_H
#define QXMPPMISTERXIQ_H

#include "qxmpp/source/QXmppIq.h"
 #include <QPointF>

extern const char* ns_misterx;

class QXmlStreamWriter;
class QDomElement;

class QXmppMisterxIq : public QXmppIq
{
public:
    QXmppMisterxIq();

    quint16 getSequence() const;
    void setSequence( quint16 seq );
    QString getSid() const;
    void setSid( const QString &sid );
    QPointF getCoordinates() const;
    void setCoordinates( const QPointF &data );
    void setPlayerList(QList<QString>);
    QList<QString> getPlayerList();

    void setData(QString);
    QString getData();

    void toXmlElementFromChild(QXmlStreamWriter *writer) const;
    void parse( QDomElement &element );

private:
    quint16 m_seq;
    QString m_sid;
    QPointF m_coordinates;
    QList<QString> m_playerlist;
};

#endif // QXMPPMISTERXIQ_H
