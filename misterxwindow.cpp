#include "misterxwindow.h"
#include "ui_misterxwindow.h"
#include "mainwindow.h"
#include "misterxsettingsform.h"

MisterXWindow::MisterXWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MisterXWindow)
{
    ui->setupUi(this);
    QCoreApplication::setOrganizationName("MisterX");
    QCoreApplication::setOrganizationDomain("epfl.ch");
    QCoreApplication::setApplicationName("mister-x-mobile");
    settings=new MainWindow();
    config= new MisterXSettingsForm();


}

MisterXWindow::~MisterXWindow()
{
    delete ui;
    delete settings;
}

void MisterXWindow::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MisterXWindow::on_hostButton_clicked()
{
    settings->show();
    settings->connectClient();

}

void MisterXWindow::on_joinButton_clicked()
{
    settings->connectClient();
}

void MisterXWindow::on_settingsButton_clicked()
{
    config->show();
}
