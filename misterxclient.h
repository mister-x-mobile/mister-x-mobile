#ifndef MISTERXCLIENT_H
#define MISTERXCLIENT_H

#include <QModelIndex>
#include <QPointF>
#include "qxmpp/source/QXmppClient.h"
#include "qxmpp/source/QXmppRoster.h"
#include "misterxengine.h"

class MisterXClient : public QXmppClient
{
     Q_OBJECT
public:
    MisterXClient( MisterxEngine *xengine,QObject *parent = 0);
    ~MisterXClient();


    QList<QString> onlineList;


    void makeConnection(void);
    void updateSettings(QList<QString>  playerList, QList<QString> areaList, QString timeString);
    void startGame();
    void stopGame(QString win);
    void result(const QVariant &value );
    void error( int code, const QString &message );

public slots:
    void createOnlineList(void);
    void addPlayer(QModelIndex);
    void removePlayer(QModelIndex);
    void updatePlayerList();
    void updateGameDat(QString coordinates);

signals:


private:
    QMap<QString, QXmppRoster::QXmppRosterEntry> entries;
    int numberOfPlayers;
    MisterxEngine * engine;


};

#endif // MISTERXCLIENT_H
